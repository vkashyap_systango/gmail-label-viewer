import views
import cron_tasks
from lib import gmail_helper

urls_name = [
              ('/auth', views.Authentication),
              ('/auth_callback', views.AuthCallback),
              ('/home', views.Home),
              ('/', views.Index),
              ('/sync_gmail_label_queue', gmail_helper.SyncLabel),
              ('/sync_gmail_message_queue', gmail_helper.SyncEmail),
              ('/getmail',views.SyncEmail),
              ('/label', views.GetLabel),
              ('/message/(.*)/(.*)', views.GetAndMarkMessage),
              ('/message/(.*)', views.GetAndMarkMessage),
              ('/workspace', views.GetAndCreateWorkspace),
              ('/delete_mail',cron_tasks.DeleteMails),
              ('/signout', views.Logout),
	     ]