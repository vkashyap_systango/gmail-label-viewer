import util
import logging
from datetime import datetime
from google.appengine.ext import db
from oauth2client.appengine import CredentialsProperty


class UserCredentials(db.Model):

  email = db.StringProperty()
  credential = CredentialsProperty()

  @staticmethod
  def create_user_credentials(input_data):
    user_credentials = UserCredentials()
    user_credentials.email = input_data['email']
    user_credentials.credential = input_data['credential']
    user_credentials.put()

  @staticmethod
  def does_email_exist(email):
    if UserCredentials.all().filter('email =', email).get() :
      return True
    return False

  @staticmethod
  def get_credential(email):
    user_data = UserCredentials.all().filter('email =', email).get()
    #user_data = UserCredentials.all().get()
    return user_data.credential

  @staticmethod
  def get_user(email):
    user = UserCredentials.all().filter('email =', email).get()
    return user
  
  @staticmethod
  def update_credential(email,new_credential):
    user_data = UserCredentials.all().filter('email =', email).get()
    user_data.credential = new_credential
    user_data.put()
    logging.info("user credential updated")


class Label(db.Model):
  user = db.ReferenceProperty(UserCredentials)
  label_id = db.StringProperty()
  label_name = db.StringProperty()
  
  @staticmethod
  def create_label(user, label_id, name):
    label = Label()
    label.user = user
    label.label_id = label_id
    label.label_name = name
    label.put()

  @staticmethod
  def get_label(user, label_id):
    label = Label.all().filter('user =', user).filter('label_id =', label_id).get()
    return label

  @staticmethod
  def get_list_of_labels(email):
    user = UserCredentials.get_user(email)
    labels = Label.all().filter('user =', user)
    label_data = {}
    label_list = []
    if labels:
      for label in labels:
        label_data['label_id'] = label.label_id
        label_data['label_name'] = label.label_name
        label_list.append(label_data.copy())
    return label_list

  @staticmethod
  def get_list_of_label_ids(email):
    user = UserCredentials.get_user(email)
    labels = Label.all().filter('user =', user)
    label_list = []
    if labels:
      for label in labels:
        label_list.append(label.label_id)
    return label_list


class Workspace(db.Model):

  user = db.ReferenceProperty(UserCredentials)
  workspace_id = db.IntegerProperty()
  workspace_name = db.StringProperty()
  label_ids = db.ListProperty(basestring)

  @staticmethod
  def create_workspace(input_data):
    workspace = Workspace()
    workspace.user = input_data['user']
    workspace.workspace_id = input_data['workspace_id']
    workspace.workspace_name = input_data['workspace_name']
    workspace.label_ids = input_data['label_ids']
    workspace.put()
  
  @staticmethod
  def does_workspace_exist(user, workspace_id):
    if Workspace.all().filter('user =',user).filter('workspace_id =', workspace_id).get():
      return True
    return False
  
  @staticmethod
  def update_workspce(input_data):
    workspace = Workspace.all().filter('user =', input_data['user']).filter('workspace_id =', input_data['workspace_id']).get()
    if workspace:
      workspace.workspace_name = input_data['workspace_name']
      workspace.label_ids = input_data['label_ids']
      workspace.put()

  @staticmethod
  def get_all_workspaces(email):
    user = UserCredentials.get_user(email)
    workspaces = Workspace.all().filter('user =', user)
    workspace_list = []
    if workspaces:
      for workspace in workspaces:
        label_ids = workspace.label_ids
        labels =[]
        for label_id in label_ids:
          label = Label.get_label(user,label_id)
          label_data = {
                        'label_id': label.label_id, 
                        'label_name': label.label_name
                        }
          labels.append(label_data)
        workspace_data = {
                          'workspace_id': workspace.workspace_id,
                          'workspace_name': workspace.workspace_name,
                          'labels': labels
                          }
        workspace_list.append(workspace_data)  
      if workspace_list:                  
       return workspace_list
      else:
        return {"response": "no workspace exist"}


class Mail(db.Model):

  user = db.ReferenceProperty(UserCredentials)
  label_ids = db.ListProperty(basestring)
  message_id = db.StringProperty()
  subject = db.StringProperty(multiline=True)
  message_body = db.TextProperty()
  sent_from = db.StringProperty()
  read = db.BooleanProperty()
  received = db.DateTimeProperty()

  @staticmethod
  def create_mail(input_data):
    valid_data = util.is_valid_dictionary(input_data)
    if not valid_data:
      mail = Mail()
      mail.user = input_data['user']
      mail.label_ids.append(input_data['label_ids'])
      mail.message_id = input_data['message_id']
      mail.subject = input_data['subject']
      mail.sent_from = input_data['from']
      mail.message_body = input_data['message_body']
      mail.received = input_data['date']
      mail.read = False
      mail.put()
    else:
      logging.info(valid_data)

  @staticmethod
  def does_message_exist(message_id):
    if Mail.all().filter('message_id =', message_id).get():
      return True
    return False

  @staticmethod
  def get_label_id_of_mail(message_id):
    return Mail.all().filter('message_id =', message_id).get()

  @staticmethod
  def update_label_id(message_id, label_id):
    mail = Mail.all().filter('message_id =', message_id).get()
    if not label_id in mail.label_ids:
      mail.label_ids.append(label_id)
      mail.put()

  @staticmethod
  def get_list_of_message(email):
    user = UserCredentials.get_user(email)
    messages = Mail.all().filter('user =', user)
    message_data = {}
    message_list = []
    if messages:
      for message in messages:
        message_data['subject'] = message.subject
        message_data['body'] = message.message_body
        message_data['from'] = message.sent_from
        message_list.append(message_data.copy())
    return message_list
  
  @staticmethod
  def get_list_of_message_and_cursor(email, label_id, limit = 10, cursor = None):
    user = UserCredentials.get_user(email)
    messages_query = Mail.all().filter('user =', user).filter('label_ids =', label_id).filter('read =',False).order('received')
    if cursor:
      messages_query = messages_query.with_cursor(cursor)
    messages = messages_query.fetch(limit)
    cursor = messages_query.cursor()
    message_list = Mail.create_list_of_message(messages)

    return {'messages': message_list, 'cursor': cursor}
  
  @staticmethod
  def create_list_of_message(messages):
    message_data = {}
    message_list = []
    for message in messages:
        message_data['message_id'] = message.message_id
        message_data['subject'] = message.subject
        message_data['label_ids'] = message.label_ids
        message_data['message_content'] = message.message_body
        message_data['sent_from'] = message.sent_from
        message_list.append(message_data.copy())
    return message_list

  @staticmethod
  def mark_message_as_read(email, message_id):
    user = UserCredentials.get_user(email)
    mail = Mail.all().filter('user =',user).filter('message_id =',message_id).get()
    if mail:
      mail.read = True
      mail.put()
      return {'response' : "message marked as read."}
    else:
      return {'response' : "unable to mark message as read."}

  @staticmethod
  def delete_mails():
    last_month_date = util.subtract_one_month(datetime.now())
    mails = Mail.all().filter('received <', last_month_date)
    if mails:
      for mail in mails:
        mail.delete()
      logging.info("mails successfully deleted")