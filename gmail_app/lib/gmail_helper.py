import logging
import httplib2
import base64
import email
from datetime import datetime

from oauth2client.client import OAuth2WebServerFlow
from apiclient.discovery import build
from oauth2client.client import AccessTokenRefreshError, OAuth2Credentials
from apiclient.http import BatchHttpRequest
from gaesessions import get_current_session

import base
import constants
import util
import wrapper
from models import UserCredentials, Label, Mail


flow = OAuth2WebServerFlow(constants.CLIENT_ID, constants.CLIENT_SECRET,constants.SCOPE, redirect_uri= constants.REDIRECT_URI,access_type='offline')



class ConnectToGmail():
  
  @staticmethod
  def get_auth_url():
    auth_uri = flow.step1_get_authorize_url()
    return auth_uri

  @staticmethod
  def get_credential(auth_code):
    credentials = flow.step2_exchange(auth_code)
    return credentials
  
  @staticmethod
  def get_user_email(credentials):
    
    user_info_service = build(
        serviceName='oauth2', version='v2',
        http=credentials.authorize(httplib2.Http()))

    user_info = None
    try:
      user_info = user_info_service.userinfo().get().execute()
    except errors.HttpError, e:
      logging.info("-----------------------")
      logging.error('An error occurred: %s', e)
    if user_info and user_info['email']:
      return user_info['email']


  @staticmethod
  def get_labels(credentials):
    http = httplib2.Http()
    logging.info(credentials)
    http = credentials.authorize(http)
    
    if credentials.access_token_expired:
      credentials.refresh(http)
      logging.info("credential refreshed")

    
    service = build('gmail', 'v1', http = http)

    response = service.users().labels().list(userId = 'me').execute()
    labels = response['labels']
    return labels
  
  @staticmethod
  def parse_raw_mail(message):
    dic = {}
    msg_str = base64.urlsafe_b64decode(message['raw'].encode('ASCII'))
    dic['message_id'] = message['id']
    mime_msg = email.message_from_string(msg_str)
    for part in mime_msg.walk():
      if part['Date']:
        if '-' in part['Date']:
          dic['date'] = datetime.strptime(part['Date'].split("-",1)[0], '%a, %d %b %Y %H:%M:%S ')
        else:
          dic['date'] = datetime.strptime(part['Date'].split("+",1)[0], '%a, %d %b %Y %H:%M:%S ')
      if part['Subject']:
        dic['subject'] = part['Subject']
      if part['From']:
        dic['from'] = part['From']
      if part.get_content_type() == 'text/plain':
         dic['message_body'] = part.get_payload()
    return dic

  @staticmethod
  def get_message_list_by_label_id(label_id, credentials):

    def list_message(request_id, response, exception):
      if exception is not None:
        logging.info('unable to get message list.')
      else:     
        message_list.append(response)
      return response    
    

    http = httplib2.Http()
    http = credentials.authorize(http)

    if credentials.access_token_expired:
      credentials.refresh(http)
      logging.info("credential refreshed")

    message_list = []
    messages = []

    service = build('gmail', 'v1', http = http)
    response = service.users().messages().list(userId = 'me', labelIds = label_id, q = util.get_date_query()).execute()
    if 'messages' in response:
      messages.extend(response['messages'])
    if messages:
      batch = BatchHttpRequest()
      for m in messages:
        batch.add(service.users().messages().get(userId = 'me', id = m['id'], format = 'raw'), callback = list_message)
      batch.execute()
    return message_list

  @staticmethod
  def sync_all_labels(email, credentials):
    if UserCredentials.does_email_exist(email):
      labels = ConnectToGmail.get_labels(credentials)
      existing_label_ids = Label.get_list_of_label_ids(email)
      if labels:
        user = UserCredentials.get_user(email)
        for label in labels:
          if not (label['id'] in existing_label_ids) and (label['id'] == "INBOX" or label['id'].startswith('Label_')):
            Label.create_label(user,label['id'], label['name'])

  @staticmethod
  def sync_all_messages(email, credentials):
    label_list = Label.get_list_of_labels(email)
    user = UserCredentials.get_user(email)
    #logging.info(email,credentials)
    for label in label_list:
      label_id = label['label_id']
      logging.info(label_id)
      message_list = ConnectToGmail.get_message_list_by_label_id(label_id, credentials)
      if message_list:
        for message in message_list:
          message_dic = ConnectToGmail.parse_raw_mail(message)
          message_dic['user'] = user
          if Mail.does_message_exist(message_dic['message_id']):
            Mail.update_label_id(message_dic['message_id'],label['label_id'])
          else:
            message_dic['label_ids'] = label_id
            Mail.create_mail(message_dic)



class SyncLabel(base.BaseController):

  def get(self):
    email = self.request.get("email")
    credential = UserCredentials.get_credential(email)
    ConnectToGmail.sync_all_labels(email, credential)
    logging.info("sync gmail label")


class SyncEmail(base.BaseController):

  def get(self):
    email = self.request.get("email")
    credential = UserCredentials.get_credential(email)
    ConnectToGmail.sync_all_labels(email, credential)
    ConnectToGmail.sync_all_messages(email,credential)
    logging.info("sync gmail message")
    #self.redirect("/home")
    return
