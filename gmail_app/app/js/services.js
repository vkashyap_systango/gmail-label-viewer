'use strict';
/* Services */
var mailAppServices= angular.module('mailAppServices',['ngResource','angular-loading-bar']);

mailAppServices.factory('LabelListService',['$resource',
	function($resource){	 
		return $resource('/label', {}, {
			query: {method:'GET',isArray:true}
		});
	}]);

mailAppServices.factory('MailListService',['$resource',
	function($resource){  
 		return $resource('/message/:label_id/:cursor', {}, {
			query: {method:'GET', isArray:false}
		});
	}]);

mailAppServices.factory('MarkAsReadService',['$resource',
	function($resource){  
 		return $resource('/message/:messageId',{messageId:'@messageId'}, {
			save: {method:'POST'}
		});
	}]);

mailAppServices.factory('SaveWorkSpace',['$resource',
	function($resource){  
 		return $resource('/workspace',{workspace_name:'@workspace_name',workspace_id:'@workspace_id'}, {
			save: {method:'POST',params:{labels:'@labels'}}
		});
	}]);

mailAppServices.factory('GetWorkSpace',['$resource',
	function($resource){  
 		return $resource('/workspace',{}, {
			save: {method:'GET', isArray:false}
		});
	}]);


mailAppServices.factory( 'WidgetLengthProvider', function() {
  var widgetLength;

  return {
    getWidgetLength: function () {return widgetLength; },
    setWidgetLength: function ( length ) {widgetLength = length; }
  };
});
