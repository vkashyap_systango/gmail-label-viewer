'use strict';

var mailApp= angular.module('mailApp',[ 
'mailAppModule',
'mailAppServices',
'ngRoute',
'mailAppDirectives',
'mailAppfilters'
]).config(['$routeProvider', function($routeProvider) {
  $routeProvider.when(	'/get-widget-list', {
    templateUrl: 'partials/widget-list.html', 
    controller: 'MailCtrl'
  }).when(  '/view/:labelId',{
    templateUrl: 'partials/widget-detail.html', 
    controller: 'MailWidgetCtrl'
  }).when(  '/widgetview',{
    templateUrl: 'partials/widget-list.html', 
    controller: 'MailCtrl'
  }).otherwise({
    redirectTo: '/widgetview'
});
}]);




