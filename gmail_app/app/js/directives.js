'use strict';
/* Directives */

var mailAppDirectives = angular.module('mailAppDirectives', []);

var mailContainerPosition={"width":0,"height":0,"parentId":""};


mailAppDirectives.directive('widgetGenerator',function(){
  return{
    restrict:'E',
    templateUrl: 'partials/widget-view.html',
    controller:'MailWidgetCtrl'
  };
});

mailAppDirectives.directive('sidePaneView',function(){
  
  return{     
    restrict:'A',    
    templateUrl: 'partials/side-pane-view.html',
    controller:'MailCtrl'
  }; 
});

mailAppDirectives.directive('smallWidgetGenerator',function(){
  
  return{     
    restrict:'E',    
    templateUrl: 'partials/small-widget-view.html',
    controller:'MailWidgetCtrl'
  }; 
});

mailAppDirectives.directive('mailGenerator',function($templateCache){
  return{
    restrict:'A',
    templateUrl:'partials/mail-view.html',
    controller:"MailWidgetCtrl"
  };
});
 
mailAppDirectives.directive('containerStyleDirective',function(){
  return function(scope, element, attrs) {
    applyStyleOnMailContainer(element);
  };
});

var applyStyleOnMailContainer = function(element){
  var parentObj = element.parent();
  var position = $(parentObj).position(); 
  var width = $(parentObj).width();
  var height = $(parentObj).height();
  if((mailContainerPosition.parentId != $(parentObj).attr("id"))){ 
    mailContainerPosition.parentId = $(parentObj).attr("id");
    mailContainerPosition.width =  width-20; 
    mailContainerPosition.height =  height-20; 
  }else{      
    mailContainerPosition.width = mailContainerPosition.width-20;
    mailContainerPosition.height =  mailContainerPosition.height-20;
  }
  $(element).css("position","absolute");
  $(element).css("height",mailContainerPosition.height+"px");
  $(element).css("width",mailContainerPosition.width+"px");

  };


