'use strict';

/* Controllers */

var mailAppModule = angular.module('mailAppModule', ['angular-loading-bar','ui.bootstrap']);
var mailLengthForShowInWidget;

mailAppModule.controller('MailCtrl', ['$routeParams','$scope','LabelListService','$location','SaveWorkSpace','WidgetLengthProvider',function($routeParams,$scope,LabelListService,$location,SaveWorkSpace,WidgetLengthProvider){
    
    $scope.bigViewCount=0;

    $scope.LoadAllLabels = function() {
      return loadLabelList();     
    };

    $scope.getWidgetDetail = function(labelId){   
      $location.path( "/view/"+labelId );    
    };

    var loadLabelList = function(){ 
      $scope.labelListings=LabelListService.query();
      
    };

    $scope.setLength = function(length){
      WidgetLengthProvider.setWidgetLength(length);
    };

    $scope.configureLabels = function(){
      $scope.labelsForworkspace=$scope.selectedLabels;
      mailLengthForShowInWidget=$scope.widgetLength;
    };

    $scope.saveWorkspace = function(workspaceId,workspaceName){
    SaveWorkSpace.save({workspace_name:workspaceName,workspace_id:workspaceId,labels:$scope.selectedLabels})
    } ;

    $scope.LoadWidgets = function(){
      $location.path("/widgetview");
    };

    $scope.bigViewIncrement = function(){
      $scope.bigViewCount=$scope.bigViewCount+1;
      return $scope.bigViewCount;
    };

    if("/get-widget-list"==$location.path() && mailLengthForShowInWidget !=0)
      $scope.mailsList = loadLabelList();
  }
]);

 
mailAppModule.controller('MailWidgetCtrl', ['$location','$routeParams','$scope','WidgetLengthProvider','MailListService','MarkAsReadService',function($location,$routeParams,$scope,WidgetLengthProvider,MailListService,MarkAsReadService){
    $scope.count=0;   
    $scope.textLimit = TEXTLIMIT;
    $scope.labelId = $routeParams.labelId; 
    $scope.messageLimit=MESSAGELIMIT;
    $scope.messageCounter=0;
    
    $scope.loadMailsForLabel = function(labelId) {
      $routeParams.labelId=labelId;
      $scope.labelId = labelId
      if($scope.mails!=undefined){
        $routeParams.cursor=$scope.mails.cursor;
      }else{
        $routeParams.cursor=null;
      }  
      $scope.mails = MailListService.query({label_id:$routeParams.labelId,cursor:$routeParams.cursor});
    };

    $scope.increment = function(){
      $scope.count =$scope.count+1;
      return $scope.count;  
    };

    $scope.getLength = function(){
     length= WidgetLengthProvider.getWidgetLength();
     return length;
    }; 
    

    $scope.sendMarkAsRead = function(mailIndex){    
      $routeParams.messageId=$scope.mails.messages[mailIndex].message_id;
      $scope.reply= MarkAsReadService.save({messageId:$routeParams.messageId});
      $scope.messageCounter++;
      if ($scope.messageCounter==$scope.messageLimit){
          loadMoreMails();          
        }
    };

    var loadMoreMails=function(){
      var temporaryMailsHolder=$scope.mails;
      $scope.loadMailsForLabel($scope.labelId);
      $scope.mails=temporaryMailsHolder.concat($scope.mails);
      $scope.messageCounter=0;  
    };

    $scope.showMessageContentForBox = function(count){
      var result= checkMail(count); 
      return (result == undefined) ? false : result.message_content;
    };

    $scope.showSubjectForBox = function(count){
      var result= checkMail(count); 
      return (result == undefined) ? false : result.subject;
    };

    $scope.showSendersIdForBox = function(count){
      var result= checkMail(count); 
      return (result == undefined) ? false : result.sent_from;
    };
   
    $scope.getPanes = function(num) {
      return new Array(num);   
    }; 

    $scope.checkIfEmailExistforLabel = function(labelId){
      if($scope.mails.messages == undefined || $scope.mails.messages.length == 0)
        return true;
      else 
        return false
    };

    $scope.goWidgetListPage = function() {  

      $location.path("/get-widget-list");
    };  

    $scope.getRandomSpan = function(){
      return Math.floor((Math.random()*100)+1);
    };
    var checkMail = function(count){ 
      if($scope.mails != undefined && $scope.mails.messages!=undefined && $scope.mails.messages.length>0){ 
        return $scope.mails.messages[count] ;
        
      }
    };
  }
]);

mailAppModule.controller("TabsCtrl",['GetWorkSpace','$scope','$filter',function (GetWorkSpace,$scope,$filter) {
 
    $scope.workspaces =[];

    var setAllInactive = function() {
        angular.forEach($scope.workspaces, function(workspace) {
            workspace.active = false;
        });
    };
 
    var addNewWorkspace = function() {
        $scope.workspaces.push({
            id: $scope.getRandomId() ,
            name: "Workspace " ,
            active: true ,
        });
    };
    
     $scope.getRandomId = function(){
      return $filter('date')(Date.now(), "ddMMyyyyHHmmss") ;
    };

    
    
    $scope.getWorkspaces = function(){
       var loadedWorkspaces = GetWorkSpace.query(function success(){
          angular.forEach(loadedWorkspaces,function(workspace){
            $scope.workspaces.push({
              id:workspace.workspace_id,
              name:workspace.workspace_name,
              active: false,
              labelList:workspace.labels
            });
          });
       }); 
    };

    $scope.addWorkspace = function () {
        setAllInactive();
        addNewWorkspace();
    };       
 
}]);
