import webapp2
import os
import json

from google.appengine.ext.webapp import template

class BaseController(webapp2.RequestHandler):

  def render_template(
    self,
    file_name,
    template_values = {}
    ):
    path = self.template_location(file_name)
    self.response.out.write(template.render(path, template_values))

  def template_location(self, file_name):
    return os.path.join(os.path.dirname(__file__), 'app',file_name)    
    
  def response_json_data(self, data):
  	self.response.headers['Content-Type'] = 'application/json'
  	self.response.out.write(json.dumps(data))
