import webapp2
import urls


app = webapp2.WSGIApplication(urls.urls_name, debug = True)


def main():
  run_wsgi_app(app)


if __name__ == '__main__':
  main()