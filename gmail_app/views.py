import logging
import time
import base64
import email
import json

# Google app engine imports
from google.appengine.ext.webapp.util import run_wsgi_app
from gaesessions import get_current_session
from google.appengine.api.labs import taskqueue

# Own imports
import util
import base
import wrapper
from models import UserCredentials, Label, Mail, Workspace
from lib.gmail_helper import ConnectToGmail


class Index(base.BaseController):
  
  def get(self):
    session = get_current_session()
    if session.is_active():
      self.redirect("/home")
      return
    self.render_template('main.html')  


class Authentication(base.BaseController):

  def get(self):
    redirect_url = ConnectToGmail.get_auth_url()
    self.redirect(redirect_url)
    logging.info("authentication end")
    return


class AuthCallback(base.BaseController):

  def get(self):
    auth_code = self.request.get('code',None)
    if auth_code:
      credential = ConnectToGmail.get_credential(auth_code)

      email = ConnectToGmail.get_user_email(credential)
      input_data = {'email': email, 'credential': credential}
      
      session = get_current_session()
      session['email'] = email

      inputs = util.is_valid_dictionary(input_data)
     
      if not inputs:
        if UserCredentials.does_email_exist(input_data['email']):
          UserCredentials.update_credential(input_data['email'],input_data['credential'])
          logging.info("user email exist into our db")
          self.redirect("/home")
          return
        else:
          UserCredentials.create_user_credentials(input_data)
          taskqueue.add(url = '/sync_gmail_label_queue',method ='GET',params=input_data)
          taskqueue.add(url = '/sync_gmail_message_queue',method ='GET',params=input_data)
      else:
        logging.info(inputs)
        self.render_template('error.html')
        return
      
      time.sleep(5)
      #self.render_template('authcallback.html')
      logging.info("redirecting to home")
      self.redirect("/home")
      return

  
class Home(base.BaseController):

  @wrapper.login_required
  def get(self):
    session = get_current_session()
    logging.info(session['email'])
    self.render_template('index.html')
    return


class GetLabel(base.BaseController):

  @wrapper.login_required
  def get(self):
    session = get_current_session()
    labels = Label.get_list_of_labels(session['email'])
    self.response_json_data(labels)

class GetAndCreateWorkspace(base.BaseController):

  @wrapper.login_required
  def get(self):
    session = get_current_session()
    workspaces = Workspace.get_all_workspaces(session['email'])
    self.response_json_data(workspaces)

  @wrapper.login_required
  def post(self):
    session = get_current_session()
    user = UserCredentials.get_user(session['email'])
    workspace_id = int(self.request.get("workspace_id"))
    workspace_name = self.request.get("workspace_name")
    labels = self.request.get_all("labels")
    label_ids = []
    for label_string in labels:
      label = json.loads(label_string)
      label_ids.append(label['label_id'])
    input_data = {
                   'user': user,
                   'workspace_id': workspace_id,
                   'workspace_name': workspace_name,
                   'label_ids': label_ids
                 }
    if Workspace.does_workspace_exist(user, workspace_id):
      Workspace.update_workspce(input_data)
    else:
      Workspace.create_workspace(input_data)    


class GetAndMarkMessage(base.BaseController):

  @wrapper.login_required
  def get(self,label_id=None,cursor=None):
    session = get_current_session()
    message = Mail.get_list_of_message_and_cursor(session['email'],label_id,cursor=cursor)
    self.response_json_data(message)
    
  @wrapper.login_required
  def post(self,message_id = None):
    session = get_current_session()
    logging.info(message_id)
    response = Mail.mark_message_as_read(session['email'],message_id)
    self.response_json_data(response)

class SyncEmail(base.BaseController):

  @wrapper.login_required
  def get(self):
    session = get_current_session()
    email = session["email"]
    credential = UserCredentials.get_credential(email)
    ConnectToGmail.sync_all_labels(email, credential)
    logging.info("sync gmail labels")
    ConnectToGmail.sync_all_messages(email,credential)
    logging.info("sync gmail message")
    self.redirect("/home")
    return


class Logout(base.BaseController):
  
  @wrapper.login_required
  def get(self):
    session = get_current_session()
    if session.is_active():
      session.terminate()
    self.redirect("/")
    return
