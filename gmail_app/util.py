import datetime

def is_valid_dictionary(input_dic):
  dic = {}
  for key, value in input_dic.iteritems():
    if value == "":
      error_key = key+"_err"
      dic[error_key] = "Please enter " + key
  return dic

def get_date_query():
  now = datetime.datetime.now()
  y = "after:"+str(now.year)+"/"+str(now.month -1)+"/"+str(now.day)+" before:"+now.strftime("%Y/%m/%d")
  return y

def subtract_one_month(t):
  one_day = datetime.timedelta(days=1)
  one_month_earlier = t - one_day
  while one_month_earlier.month == t.month or one_month_earlier.day > t.day:
      one_month_earlier -= one_day
  return one_month_earlier