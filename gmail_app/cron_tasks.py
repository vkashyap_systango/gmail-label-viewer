import base
import logging

from models import Mail


class DeleteMails(base.BaseController):
  def get(self):
    Mail.delete_mails()
    logging.info("cron successfully executed")