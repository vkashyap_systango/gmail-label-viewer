import base
import logging
import httplib2
from oauth2client.client import OAuth2WebServerFlow
from apiclient.discovery import build
from oauth2client.client import AccessTokenRefreshError
from apiclient.http import BatchHttpRequest

#TODO create a class for it.
flow = OAuth2WebServerFlow("772338575858-l66lbv8laucc856cmd63sbef98psadcg.apps.googleusercontent.com",
                               "8guGDP6YUMojZwGsvo2MpZio",
                               ['https://www.googleapis.com/auth/gmail.readonly',
                                 'https://mail.google.com/',
                                 'https://www.googleapis.com/auth/gmail.modify'
                               ],
                               redirect_uri='http://localhost:8080/auth_callback')
 

def get_auth_url():
  auth_uri = flow.step1_get_authorize_url()
  return auth_uri


def get_credential(auth_code):
  credentials = flow.step2_exchange(auth_code)
  return credentials



def get_labels(credentials):

  http = httplib2.Http()
  http = credentials.authorize(http)
  service = build('gmail', 'v1', http = http)

  response = service.users().labels().list(userId = 'me').execute()
  labels = response['labels']
  for label in labels:
    print 'Label id: %s - Label name: %s' % (label['id'], label['name'])
  return labels


def get_message(credentials):
  def list_message(request_id, response, exception):
    if exception is not None:
      pass
    else:
      message_list.append(response)
      print "-----------------------------"
      #print response
      pass
    return response

  
  
  http = httplib2.Http()
  http = credentials.authorize(http)
  service = build('gmail', 'v1', http = http)

  message_list = []

  response = service.users().messages().list(userId='me',maxResults=1).execute()
  batch = BatchHttpRequest()
  messages = []
  if 'messages' in response:
    messages.extend(response['messages'])

  for m in messages:
    batch.add(service.users().messages().get(userId = 'me', id = m['id']), callback = list_message)
    print m

  batch.execute()
  #print message_list
  print "Finish============"
  return message_list


      
    