from gaesessions import get_current_session

def login_required(func):
  def inner(*s):
    session = get_current_session()
    if not session.is_active():
      s[0].redirect("/")
      return
    func(*s)
  return inner